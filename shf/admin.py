from django.contrib import admin
from . import models

admin.site.register(models.Stadium)
admin.site.register(models.Area)
admin.site.register(models.Condition)
admin.site.register(models.Fee)
admin.site.register(models.Hotel)
