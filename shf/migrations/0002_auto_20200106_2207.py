# Generated by Django 3.0.2 on 2020-01-06 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shf', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Condition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('condition', models.IntegerField(choices=[(1, 'ショッピング'), (2, '娯楽施設'), (3, '温泉'), (4, '日本文化体験'), (5, '食の名産'), (6, 'テーマパーク'), (7, '景色')])),
            ],
        ),
        migrations.CreateModel(
            name='Fee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fee', models.IntegerField(choices=[(1, '~1万'), (2, '1~2万'), (3, '2~3万'), (4, '3~4万'), (5, '4~5万'), (6, '5万~')])),
            ],
        ),
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('detail', models.TextField()),
                ('image1', models.ImageField(blank=True, null=True, upload_to='')),
                ('image2', models.ImageField(blank=True, null=True, upload_to='')),
                ('image3', models.ImageField(blank=True, null=True, upload_to='')),
                ('access', models.TextField()),
                ('booking', models.URLField(max_length=500)),
                ('area', models.ManyToManyField(to='shf.Area')),
                ('condition', models.ManyToManyField(to='shf.Condition')),
                ('fee', models.ManyToManyField(to='shf.Fee')),
            ],
        ),
        migrations.AddField(
            model_name='area',
            name='condition',
            field=models.ManyToManyField(to='shf.Condition'),
        ),
        migrations.AddField(
            model_name='area',
            name='fee',
            field=models.ManyToManyField(to='shf.Fee'),
        ),
    ]
