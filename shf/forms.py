from django import forms
from . import models


class SearchForm(forms.Form):
    stadium = forms.ModelChoiceField(queryset=models.Stadium.objects,
                                     label='Stadium',
                                     empty_label='(Select From Here)',
                                     help_text='Select your venue what you watch the competition.',
                                     required=True
                                     )

    fee = forms.ChoiceField(choices=models.Fee.FEES,
                            label='Price',
                            widget=forms.RadioSelect,
                            help_text='You can choose only one.',
                            required=True
                            )

    condition = forms.MultipleChoiceField(choices=models.Condition.CONDITIONS,
                                          label='Condition',
                                          widget=forms.CheckboxSelectMultiple,
                                          help_text='The conditions you want for your staying area'
                                                    '(multiple choice allowed).',
                                          required=True
                                          )
