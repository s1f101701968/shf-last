from django.urls import path
from . import views

app_name = 'shf'

urlpatterns = [
    path('', views.search, name="search"),
    path('area/<int:pk>/', views.AreaDetailView.as_view(), name="area"),
    path('hotel/<int:pk>/', views.HotelDetailView.as_view(), name='hotel')
]
