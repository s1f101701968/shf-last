from django.shortcuts import render, redirect, get_object_or_404
from .forms import SearchForm
from . import models
from django.views.generic import DetailView


def search(request):
    form = SearchForm(request.GET)
    if form.is_valid():
        stadium = form.cleaned_data['stadium']
        fee = form.cleaned_data['fee']
        condition = form.cleaned_data['condition']
        area_list = models.Area.objects.filter(stadium=stadium, fee=fee, condition=condition[0])
        for i in range(1, len(condition)):
            area_list = area_list.filter(condition=condition[i])
        context = {'area_list': area_list}
        return render(request, 'shf/result.html', context)
    else:
        form = SearchForm()
    context = {'search_form': form}
    return render(request, 'shf/top.html', context)


class AreaDetailView(DetailView):
    model = models.Area
    template_name = 'shf/area.html'


class HotelDetailView(DetailView):
    model = models.Hotel
    template_name = 'shf/hotel.html'
