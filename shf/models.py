from django.db import models


class Stadium(models.Model):
    stadium = models.CharField(max_length=50)

    def __str__(self):
        return self.stadium


class Condition(models.Model):
    CONDITIONS = (
        (1, "Shopping"),
        (2, "Amusement Facilities"),
        (3, "Hot Spring"),
        (4, "Japanese Culture Experience"),
        (5, "Food"),
        (6, "Amusement Park"),
        (7, "Landscape"),
    )
    condition = models.IntegerField(
        choices=CONDITIONS
    )

    def __str__(self):
        return self.get_condition_display()


class Fee(models.Model):
    FEES = (
        (1, "~Ten thousand"),
        (2, "Ten~Twenty thousand"),
        (3, "Twenty~Thirty thousand"),
        (4, "Thirty~Forty thousand"),
        (5, "Forty~Fifty thousand"),
        (6, "Fifty thousand ~"),
    )
    fee = models.IntegerField(
        choices=FEES
    )

    def __str__(self):
        return self.get_fee_display()


class Area(models.Model):
    area = models.CharField(max_length=10)
    stadium = models.ManyToManyField(Stadium)
    condition = models.ManyToManyField(Condition)
    fee = models.ManyToManyField(Fee)
    image = models.ImageField(upload_to='shf/', blank=True, null=True)
    image1 = models.ImageField(upload_to='shf/', blank=True, null=True)
    image2 = models.ImageField(upload_to='shf/', blank=True, null=True)
    spot1 = models.URLField(max_length=500, default="")
    spot2 = models.URLField(max_length=500, default="")
    spot_name1 = models.CharField(max_length=30, default="")
    spot_name2 = models.CharField(max_length=30, default="")
    desc1 = models.TextField(default="")
    desc2 = models.TextField(default="")

    def __str__(self):
        return self.area


class Hotel(models.Model):
    name = models.CharField(max_length=100)
    area = models.ManyToManyField(Area)
    condition = models.ManyToManyField(Condition)
    fee = models.IntegerField(default=0)
    detail = models.TextField()
    image1 = models.ImageField(upload_to='shf/', blank=True, null=True)
    image2 = models.ImageField(upload_to='shf/', blank=True, null=True)
    image3 = models.ImageField(upload_to='shf/', blank=True, null=True)
    access = models.TextField()
    booking = models.URLField(max_length=500)

    def __str__(self):
        return self.name
